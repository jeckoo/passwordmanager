﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PasswordManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DataTable pswTable;

        public string PasswordPath
        {
            get
            {
                string user = (UserSettings.UserName.GetHashString() + UserSettings.PasswordUser.GetHashString()).GetHashString();
                Directory.CreateDirectory("./Users");
                return $"./Users/{user}";
            }
        }

        public Settings UserSettings { get; set; }
        private static MainWindow instance;

        public static MainWindow GetInstance()
        {
            return instance;
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            instance = this;

            UserSettings = new Settings();
            UserSettings.ShowDialog();



            pswTable = new DataTable("test");
            pswTable.Columns.Add("Nome");
            pswTable.Columns.Add("Password");
            dataGrid.ItemsSource = pswTable.DefaultView;
        }

        private void LoadFile(object sender, RoutedEventArgs e)
        { 
            try
            {
                string user = (UserSettings.UserName.GetHashString() + UserSettings.PasswordUser.GetHashString()).GetHashString();
                Directory.CreateDirectory("./Users");
                var file = new StreamReader(File.OpenRead($"./Users/{user}"));
                string s = file.ReadToEnd().Decrypt(UserSettings.PasswordFile);
                file.Close();
                pswTable = JsonConvert.DeserializeObject(s, typeof(DataTable)) as DataTable;
                dataGrid.ItemsSource = pswTable.DefaultView;
                dataGrid.Items.Refresh();
            }
            catch(Exception ex)
            {
                if(ex.Message.Contains("Could not find file"))
                {
                    MessageBox.Show("Errore: Username o password errata");
                }
                else
                    MessageBox.Show("Errore: " + ex.Message);
            }
        }

        private void newButton_Click(object sender, RoutedEventArgs e)
        {
            pswTable = new DataTable("test");
            pswTable.Columns.Add("Nome");
            pswTable.Columns.Add("Password");
            dataGrid.ItemsSource = pswTable.DefaultView;
            dataGrid.Items.Refresh();
        }

        private void CopyToClipboard(object sender, RoutedEventArgs e)
        {
            var Sender = (Button)sender;
            string psw = Sender.CommandParameter as string;
            try
            {
                Clipboard.SetText(psw.Decrypt(UserSettings.PasswordUser));
            }
            catch
            {
                MessageBox.Show("Errore: password errata!");
            }
        }

        private void PswClick(object sender, RoutedEventArgs e)
        {
            Button Sender = sender as Button;
            dynamic Context = Sender.DataContext;
            var psw = (Sender.Content as PasswordBox).Password.Encrypt(UserSettings.PasswordUser);
            Context["Password"] = psw;
            
        }

        private void NewPassword(object sender, RoutedEventArgs e)
        {
            NewPassword nPsw = new NewPassword();
            nPsw.ShowDialog();

            if(string.IsNullOrEmpty(nPsw.Nome))
            {
                MessageBox.Show("Non si puo' inserire una password senza un nome abbinato!");
            }
            else if(string.IsNullOrEmpty(nPsw.Psw))
            {
                MessageBox.Show("Non si puo' inserire una password vuota!");
            }
            else
            {
                var row = pswTable.NewRow();
                row.BeginEdit();
                row["Nome"] = nPsw.Nome;
                row["Password"] = nPsw.Psw;
                row.EndEdit();
                pswTable.Rows.Add(row);
                dataGrid.ItemsSource = null;
                dataGrid.ItemsSource = pswTable.DefaultView;
                dataGrid.Items.Refresh();
            }
        }

        private void SaveFile(object sender, RoutedEventArgs e)
        {
            var file = File.OpenWrite(PasswordPath);
            string json = JsonConvert.SerializeObject(pswTable);
            string crypt = json.Encrypt(UserSettings.PasswordFile);
            file.Write(Encoding.UTF8.GetBytes(crypt), 0, crypt.Count());
            file.Close();
        }




        private void Window_Closed(object sender, EventArgs e)
        {
            UserSettings.Close();
        }

        private void OpenSettings(object sender, RoutedEventArgs e)
        {
            UserSettings.ShowDialog();
        }
    }
}
