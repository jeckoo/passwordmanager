﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PasswordManager
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        #region [Properties]
        public string PasswordUser
        {
            get
            {
                return passwordPBox.Password.GetHashString();
            }
        }

        public string PasswordFile
        {
            get
            {
                return passwordFBox.Password.GetHashString();
            }
        }

        public string UserName
        {
            get
            {
                return usernameBox.Text;
            }
        }
        #endregion

        #region [Magic stuff]
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }
        #endregion

        public Settings()
        {
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(usernameBox.Text))
            {
                MessageBox.Show("Insert a user name!");
                return;
            }
            if(string.IsNullOrEmpty(passwordFBox.Password) )
            {
                MessageBox.Show("Insert a crypt password!");
                return;
            }
            if(string.IsNullOrEmpty(passwordPBox.Password))
            {
                MessageBox.Show("Insert a user password!");
                return;
            }

            this.Hide();
        }

    }
}
